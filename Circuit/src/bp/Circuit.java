package bp;
/**
 * The business class for circuit.
 * This also implements the ICircuit class.
 * @author austi
 *
 */
public class Circuit implements ICircuit {
	/**
	 * Set the number of digits to 4.
	 */
	public static final int NUMBER_OF_DIGITS = 4;
	/**
	 * Define the voltage.
	 */
	private double voltage;
	/**
	 * Define the amperage.
	 */
	private double amperage;
	/** 
	 * Define the resistance.
	 */
	private double resistance;
	/**
	 * This is a variable representing 10.
	 */
	private final int ten = 10;

	
	@Override
	public final double getVoltage() {
		return Math.round(voltage * Math.pow(ten, NUMBER_OF_DIGITS)) 
				/ Math.pow(ten, NUMBER_OF_DIGITS);
	}

	@Override
	public final double getAmperage() {
		return Math.round(amperage * Math.pow(ten, NUMBER_OF_DIGITS)) 
				/ Math.pow(ten, NUMBER_OF_DIGITS);
	}

	@Override
	public final double getResistance() {
		return Math.round(resistance * Math.pow(ten, NUMBER_OF_DIGITS)) 
				/ Math.pow(ten, NUMBER_OF_DIGITS);
	}

	@Override
	public final void setVoltage(final double pVoltage) {
		voltage = pVoltage;
		
	}

	@Override
	public final void setAmperage(final double pAmperage) {
		amperage = pAmperage;
		
	}

	@Override
	public final void setResistance(final double pResistance) {
		resistance = pResistance;
		
	}

	@Override
	public final void calculateVoltage() {
		voltage = amperage * resistance;
		
	}

	@Override
	public final void calculateAmperage() {
		amperage = voltage / resistance;
		
	}

	@Override
	public final void calculateResistance() {
		resistance = voltage / amperage;
		
	}

}
