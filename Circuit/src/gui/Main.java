package gui;
import java.util.Scanner;
import bp.Circuit;
import bp.ICircuit;
/*
 //kill the ability to construct this class
		//from another file.

 //Define variable userChoice.
 //This is a variable representing the number 3.
  
  //Display the purpose statement.
 
 //Ask user what they want to calculate.
  	//save userChoice
  //if user wants to calculate amperage
     //ask for voltage
      	//save voltage
    //ask for resistance
      	//save resistance
   //calculate amperage
   //display answer back to user
  
  //else if user wants to calculate voltage
      //ask for amperage
      	//save amperage
      //ask for resistance
      	//save resistance
     //calculate voltage
     //display answer back to user
  
  //else if user wants to calculate resistance
      //ask for amperage
       //save amperage
     //ask for voltage
      //save voltage
     //calculate resistance
     //display answer back to user
  
  //close keyboard
 */
/**
 * This is the main class.
 * @author austi
 *
 */
public final class Main {
	/**
	 * Constructor.
	 * @param args
	 */
	private Main() {
		//kill the ability to construct this class
		//from another file.
	}
	/**
	 * 
	 * @param args args
	 */
	public static void main(final String[] args) {
		Scanner keyboard = new Scanner(System.in);
		ICircuit myCircuit = new Circuit();

		//Define variable userChoice.
		int userChoice;
		//This is a variable representing the number 3.
		final int three = 3;
	//Display the purpose statement.
		System.out.println("We will calculate either the amperage, "
				+ "voltage, or resistance of a circuit "
				+ "given the other two parameters.");
		
	//ask user what they want to calculate
		System.out.println("Would you like to calculate 1) amperage,"
				+ " 2) voltage, or 3) resistance?");
			//save userChoice
			userChoice = keyboard.nextInt();
			
	//if user wants to calculate amperage   
		if (userChoice == 1) {
		//ask for voltage
			System.out.println("Please enter the voltage.");
		//save voltage
			myCircuit.setVoltage(keyboard.nextDouble());
		//ask for resistance
			System.out.println("Please enter resistance.");
		//save resistance
			myCircuit.setResistance(keyboard.nextDouble());
		//calculate amperage
			myCircuit.calculateAmperage();
		//display answer back to user
			System.out.println("The total amperage is: " 
			+ myCircuit.getAmperage() + " amps.");
		
	//else if user wants to calculate voltage	
		} else if (userChoice == 2) {
		//ask for amperage
			System.out.println("Please enter amperage.");
		//save amperage
			myCircuit.setAmperage(keyboard.nextDouble());
		//ask for resistance
			System.out.println("Please enter resistance.");
		//save resistance
			myCircuit.setResistance(keyboard.nextDouble());
		//calculate voltage
			myCircuit.calculateVoltage();
		//display answer back to user
			System.out.println("The total voltage is: "
			+ myCircuit.getVoltage() + " volts.");

	//else if user wants to calculate resistance     
		} else if (userChoice == three) {
		//ask for amperage
			System.out.println("Please enter amperage.");
		//save amperage
			myCircuit.setAmperage(keyboard.nextDouble());
		//ask for voltage
			System.out.println("Please enter voltage.");
		//save voltage
			myCircuit.setVoltage(keyboard.nextDouble());
		//calculate resistance
			myCircuit.calculateResistance();
		//display answer back to user
			System.out.println("The total resistance is: " 
			+ myCircuit.getResistance() + " ohms.");
		}
		
		
		//close keyboard
		keyboard.close();
	}

}
