package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import bp.Circuit;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class MainGui {

	private JFrame frame;
	private JTextField txtAmperage;
	private JTextField txtVoltage;
	private JTextField txtResistance;
	private JTextField txtResults;
	private JButton btnCalculate;

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGui window = new MainGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 705, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JLabel lblThisProgramWill = new JLabel("<html>\r\nThis program will calculate the amperage, voltage, or resistance of a circuit given that two are known.\r\n</html>");
		lblThisProgramWill.setForeground(Color.DARK_GRAY);
		lblThisProgramWill.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblThisProgramWill.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(lblThisProgramWill, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.SOUTH);
		
		
		btnCalculate = new JButton();
		btnCalculate.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				if (arg0.getKeyChar() == KeyEvent.VK_ESCAPE) {
					txtResistance.setText("");
					txtVoltage.setText("");
					txtAmperage.setText("");
					txtResults.setText("");
					btnCalculate.setEnabled(false);
					txtResistance.requestFocus();
				}
			}
		});
		btnCalculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
					Circuit main = new Circuit();
					if (txtAmperage.getText().isEmpty()) {
						main.setVoltage(Double.parseDouble(txtVoltage.getText()));
						main.setResistance(Double.parseDouble(txtResistance.getText()));
						main.calculateAmperage();
						txtResults.setText(Double.toString(main.getAmperage()));
					} else if (txtResistance.getText().isEmpty()) {
						main.setVoltage(Double.parseDouble(txtVoltage.getText()));
						main.setAmperage(Double.parseDouble(txtAmperage.getText()));
						main.calculateResistance();
						txtResults.setText(Double.toString(main.getResistance()));
					} else if (txtVoltage.getText().isEmpty()) {
						main.setResistance(Double.parseDouble(txtResistance.getText()));
						main.setAmperage(Double.parseDouble(txtAmperage.getText()));
						main.calculateVoltage();
						txtResults.setText(Double.toString(main.getVoltage()));
					}
				
			}
		});
		btnCalculate.setText("Calculate");

		btnCalculate.setToolTipText("This is the calculate button.");
		btnCalculate.setEnabled(false);
		
		btnCalculate.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel.add(btnCalculate);
		
		JPanel panel_1 = new JPanel();
		frame.getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(55, 153, 226, 32);
		panel_1.add(panel_2);
		
		JLabel lblAmperage = new JLabel("Amperage:");
		lblAmperage.setForeground(Color.RED);
		lblAmperage.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel_2.add(lblAmperage);
		
		txtAmperage = new JTextField();
		txtAmperage.setToolTipText("Enter your known amperage here.");
		txtAmperage.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent arg0) {
				if (arg0.getKeyChar() < KeyEvent.VK_0 || arg0.getKeyChar() > KeyEvent.VK_9) {
					if (arg0.getKeyChar() != KeyEvent.VK_PERIOD) {
						arg0.consume();
					}
				} if (arg0.getKeyChar() == KeyEvent.VK_PERIOD && txtAmperage.getText().contains(".")) {
					arg0.consume();
				}
				if (arg0.getKeyChar() == KeyEvent.VK_ESCAPE) {
					txtResistance.setText("");
					txtVoltage.setText("");
					txtAmperage.setText("");
					txtResults.setText("");
					btnCalculate.setEnabled(false);
					txtResistance.requestFocus();
				}
				
			}
			@Override
			public void keyReleased(KeyEvent e) {
				setCalculateEnabled();
			}
		});
		panel_2.add(txtAmperage);
		txtAmperage.setColumns(10);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(55, 221, 248, 32);
		panel_1.add(panel_3);
		
		JLabel lblVoltage = new JLabel("Voltage:");
		lblVoltage.setForeground(Color.GREEN);
		lblVoltage.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel_3.add(lblVoltage);
		
		txtVoltage = new JTextField();
		txtVoltage.setToolTipText("Enter your known voltage here.");
		txtVoltage.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent arg0) {
				if (arg0.getKeyChar() < KeyEvent.VK_0 || arg0.getKeyChar() > KeyEvent.VK_9) {
					if (arg0.getKeyChar() != KeyEvent.VK_PERIOD) {
						arg0.consume();
					}
				} if (arg0.getKeyChar() == KeyEvent.VK_PERIOD && txtVoltage.getText().contains(".")) {
					arg0.consume();
				}
				if (arg0.getKeyChar() == KeyEvent.VK_ESCAPE) {
					txtResistance.setText("");
					txtVoltage.setText("");
					txtAmperage.setText("");
					txtResults.setText("");
					btnCalculate.setEnabled(false);
					txtResistance.requestFocus();
				}
			}
		
			@Override
			public void keyReleased(KeyEvent e) {
				setCalculateEnabled();
			}
		});
		panel_3.add(txtVoltage);
		txtVoltage.setColumns(10);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBounds(55, 81, 226, 32);
		panel_1.add(panel_4);
		
		JLabel lblResistance = new JLabel("Resistance:");
		lblResistance.setForeground(Color.BLUE);
		lblResistance.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel_4.add(lblResistance);
		
		txtResistance = new JTextField();
		txtResistance.setToolTipText("Enter your known resistance here.");
		txtResistance.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent arg0) {
				if (arg0.getKeyChar() < KeyEvent.VK_0 || arg0.getKeyChar() > KeyEvent.VK_9) {
					if (arg0.getKeyChar() != KeyEvent.VK_PERIOD) {
						arg0.consume();
					}
				} if (arg0.getKeyChar() == KeyEvent.VK_PERIOD && txtResistance.getText().contains(".")) {
					arg0.consume();
				}
				if (arg0.getKeyChar() == KeyEvent.VK_ESCAPE) {
					txtResistance.setText("");
					txtVoltage.setText("");
					txtAmperage.setText("");
					txtResults.setText("");
					btnCalculate.setEnabled(false);
					txtResistance.requestFocus();
				}
			} 
			@Override
			public void keyReleased(KeyEvent e) {
				setCalculateEnabled();
			}
		});
		panel_4.add(txtResistance);
		txtResistance.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Please enter your numbers in two of the boxes to calculate the other.");
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(107, 28, 511, 16);
		panel_1.add(lblNewLabel);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBounds(398, 130, 248, 32);
		panel_1.add(panel_5);
		
		JLabel lblResults = new JLabel("Results:");
		lblResults.setForeground(Color.ORANGE);
		lblResults.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel_5.add(lblResults);
		
		txtResults = new JTextField();
		txtResults.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				if (arg0.getKeyChar() == KeyEvent.VK_ESCAPE) {
					txtResistance.setText("");
					txtVoltage.setText("");
					txtAmperage.setText("");
					txtResults.setText("");
					btnCalculate.setEnabled(false);
					txtResistance.requestFocus();
				}
			}
		});
		txtResults.setToolTipText("This is your answer for your calculation.");
		txtResults.setEditable(false);
		panel_5.add(txtResults);
		txtResults.setColumns(10);
		
		
		frame.getRootPane().setDefaultButton(btnCalculate);
	    
	    
		
		JButton btnClear = new JButton("Clear");
		btnClear.setMnemonic(KeyEvent.VK_ESCAPE);
		btnClear.setToolTipText("Clear all entry.");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtAmperage.setText("");
				txtResistance.setText("");
				txtVoltage.setText("");
				txtResults.setText("");
				txtResistance.requestFocus();
			}
			
		});
		btnClear.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel.add(btnClear);
		
		
		
		
	}
	private void setCalculateEnabled() {
		int numberOfBoxesWithData = 0;
		
		if (txtAmperage.getText().length() > 0) {
			try {
				Double.parseDouble(txtAmperage.getText());
				numberOfBoxesWithData++;
			} catch (Exception e) {
				
			}
		}
		if (txtVoltage.getText().length() > 0) {
			try {
				Double.parseDouble(txtVoltage.getText());
				numberOfBoxesWithData++;
			} catch (Exception e) {
				
			}
		}
		if (txtResistance.getText().length() > 0) {
			try {
				Double.parseDouble(txtResistance.getText());
				numberOfBoxesWithData++;
			} catch (Exception e) {
				
			}
		}
		if (numberOfBoxesWithData == 2) {
			btnCalculate.setEnabled(true);
		} else {
			btnCalculate.setEnabled(false);
		}
		
	}
}
