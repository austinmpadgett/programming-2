package bp;

import java.security.InvalidParameterException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import db.Database;
import db.Parameter;

/**
 * This is the Circuit class that implements ICiruit and IpermanentStorage.
 * 
 * @author 183299
 *
 */
public class Circuit implements ICircuit, IPermanentStorage {
	/**
	 * This is a variable for toyID.
	 */
	private int toyID;
	/**
	 * This is a variable for circuitID.
	 */
	private int circuitID;
	/**
	 * This is a variable for voltage.
	 */
	private double voltage;
	/**
	 * This is a variable for amperage.
	 */
	private double amperage;
	/**
	 * This is a variable for resistance.
	 */
	private double resistance;
	/**
	 * This is a string variable for manufactureLocation.
	 */
	private String manufactureLocation;

	/**
	 * 
	 * @param pCircuitID
	 *            pCircuitID
	 */
	public Circuit(final int pCircuitID) {
		circuitID = pCircuitID;
	}

	@Override
	public final int getToyID() {
		return toyID;
	}

	@Override
	public final int getCircuitID() {
		return circuitID;
	}

	@Override
	public final double getVoltage() {
		return voltage;
	}

	@Override
	public final double getAmperage() {
		return amperage;
	}

	@Override
	public final double getResistance() {
		return resistance;
	}

	@Override
	public final String getManufactureLocation() {
		return manufactureLocation;
	}

	@Override
	public final void setToyID(final int pToyID) {
		toyID = pToyID;

	}

	@Override
	public final void setCircuitID(final int pCircuitID) {
		if (pCircuitID != 1 && pCircuitID != 2) {
			throw new InvalidParameterException("Circuit ID must be one or two.");
		} else {
			circuitID = pCircuitID;
		}
	}

	@Override
	public final void setVoltage(final double pVoltage) {
		voltage = pVoltage;

	}

	@Override
	public final void setAmperage(final double pAmperage) {
		amperage = pAmperage;

	}

	@Override
	public final void setResistance(final double pResistance) {
		resistance = pResistance;

	}

	@Override
	public final void setManufactureLocation(final String pManufactureLocation) {
		manufactureLocation = pManufactureLocation;

	}

	@Override
	public final void calculateVoltage() {
		voltage = amperage * resistance;

	}

	@Override
	public final void calculateAmperage() {
		amperage = voltage / resistance;
	}

	@Override
	public final void calculateResistance() {
		resistance = voltage / amperage;

	}

	@Override
	public final boolean isValid() {
		return voltage == amperage * resistance;
	}

	@Override
	public final void save() {
		Database db = new Database(Settings.getServer());
		List<Parameter> params = new ArrayList<>();

		// set the parameter values from the properties
		params.add(new Parameter<Integer>(toyID));
		params.add(new Parameter<Integer>(circuitID));
		params.add(new Parameter<String>(manufactureLocation));
		params.add(new Parameter<Double>(voltage));
		params.add(new Parameter<Double>(amperage));
		params.add(new Parameter<Double>(resistance));

		// now save!!!!
		db.executeSql("usp_SaveCircuit", params);
	}

	@Override
	public void clear() {

	}

	@Override
	public void delete() {

	}

	@Override
	public final void load(final int... id) {
		Database db = new Database(Settings.getServer());
		List<Parameter> params = new ArrayList<>();
		params.add(new Parameter<Integer>(id[0]));
		params.add(new Parameter<Integer>(id[1]));

		ResultSet circuits = db.getResultSet("usp_GetCircuit", params);

		try {
			if (circuits.next()) {
				toyID = circuits.getInt("ToyID");
				circuitID = circuits.getInt("CircuitID");
				manufactureLocation = circuits.getString("ManufactureLocation");
				amperage = circuits.getDouble("Amperage");
				voltage = circuits.getDouble("Voltage");
				resistance = circuits.getDouble("Resistance");
			} else {
				throw new InvalidParameterException("CircuitID not found");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
