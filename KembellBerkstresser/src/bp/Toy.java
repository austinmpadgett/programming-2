package bp;

import java.security.InvalidParameterException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import db.Database;
import db.Parameter;

/**
 * This class is named toy that implements IToy and IPermanentStorage.
 * 
 * @author 183299
 *
 */
public class Toy implements IToy, IPermanentStorage {
	/**
	 * This is a variable for toyID.
	 */
	private int toyID;
	/**
	 * This is a string for inspector.
	 */
	private String inspector;
	/**
	 * This is a variable date for inspectionDateTime.
	 */
	private Date inspectionDateTime;
	/**
	 * This is the circuit for the toys.
	 */
	private Circuit circuit1 = new Circuit(1);
	/**
	 * This is the circuit for the toys.
	 */
	private Circuit circuit2 = new Circuit(2);

	@Override
	public final int getToyID() {
		return toyID;
	}

	@Override
	public final String getInspector() {
		return inspector;
	}

	@Override
	public final Date getInspectionDateTime() {
		return inspectionDateTime;
	}

	@Override
	public final Circuit getCircuit1() {
		return circuit1;
	}

	@Override
	public final Circuit getCircuit2() {
		return circuit2;
	}

	@Override
	public final void setToyID(final int pToyID) {
		toyID = pToyID;
		circuit1.setToyID(pToyID);
		circuit2.setToyID(pToyID);
	}

	@Override
	public final void setInspector(final String pInspector) {
		inspector = pInspector;
	}

	@Override
	public final void setInspectionDateTime(final Date pInspectionDateTime) {
		inspectionDateTime = pInspectionDateTime;
	}

	@Override
	public final void setCircuit1(final Circuit pCircuit1) {
		circuit1 = pCircuit1;
	}

	@Override
	public final void setCircuit2(final Circuit pCircuit2) {
		circuit2 = pCircuit2;
	}

	@Override
	public final void save() {
		Database db = new Database(Settings.getServer());
		List<Parameter> params = new ArrayList<>();

		// set the parameter values from the properties
		params.add(new Parameter<Integer>(toyID));
		params.add(new Parameter<String>(inspector));
		params.add(new Parameter<Date>(inspectionDateTime));

		db.executeSql("usp_SaveToy", params);
		circuit1.save();
		circuit2.save();
	}

	@Override
	public void clear() {

	}

	@Override
	public final void delete() {
		Database db = new Database(Settings.getServer());
		List<Parameter> params = new ArrayList<>();

		params.add(new Parameter<Integer>(toyID));

		db.executeSql("usp_DeleteToy", params);

	}

	@Override
	public final void load(final int... id) {
		Database db = new Database(Settings.getServer());
		List<Parameter> params = new ArrayList<>();

		params.add(new Parameter<Integer>(id[0]));

		ResultSet toys = db.getResultSet("usp_GetToy", params);

		try {
			if (toys.next()) {
				toyID = toys.getInt("ToyID");
				inspectionDateTime = toys.getTimestamp("InspectionDateTime");
				inspector = toys.getString("Inspector");
				circuit1.load(toyID, 1);
				circuit2.load(toyID, 2);
			} else {
				throw new InvalidParameterException("ToyID not found.");
			}
		} catch (SQLException e) {

			JOptionPane.showMessageDialog(null, "ToyID not found!");
			e.getMessage();
		}
	}

}
