package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import bp.Toy;

import javax.swing.JPanel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.security.InvalidParameterException;

import javax.swing.JComboBox;

public class Main {
	public static final String[] COUNTRIES = { "China", "Germany", "United States" };
	private JFrame frame;
	private JTextField txtInspector;
	private JTextField txtDate;
	private JTextField txtVoltage1;
	private JTextField txtResistance1;
	private JTextField txtVoltage2;
	private JTextField txtResistance2;
	private JTextField txtToyID;
	private JButton btnDelete;
	private JButton btnLoad;
	private JButton btnSave;

	Toy myToy = new Toy();
	private JComboBox<String> comboBox;
	private JComboBox<String> comboBox_1;

	/**
	 * Launch the applicant private JComboBox comboBox_1; private JComboBox
	 * comboBox;
	 * 
	 * /** Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	// view
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 618, 559);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblInspector = new JLabel("Inspector:");
		lblInspector.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblInspector.setBounds(89, 58, 102, 16);
		frame.getContentPane().add(lblInspector);

		txtInspector = new JTextField();
		txtInspector.addKeyListener(new TxtInspectorKeyListener());

		txtInspector.setToolTipText("Enter your inspection name.");
		txtInspector.setBounds(203, 56, 206, 22);
		frame.getContentPane().add(txtInspector);
		txtInspector.setColumns(10);

		JLabel lblDate = new JLabel("Date:");
		lblDate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDate.setBounds(89, 165, 56, 16);
		frame.getContentPane().add(lblDate);

		txtDate = new JTextField();
		txtDate.setEnabled(false);
		txtDate.setText("<calculated>");
		txtDate.setBounds(203, 163, 206, 22);
		frame.getContentPane().add(txtDate);
		txtDate.setColumns(10);

		JPanel panel = new JPanel();
		panel.setBounds(39, 230, 268, 193);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblV = new JLabel("Voltage");
		lblV.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblV.setBounds(12, 57, 80, 16);
		panel.add(lblV);

		txtVoltage1 = new JTextField();
		txtVoltage1.setToolTipText("Enter voltage for Circuit 1.");
		txtVoltage1.addKeyListener(new TxtDoubleFieldKeyListener());
		txtVoltage1.setBounds(114, 55, 116, 22);
		panel.add(txtVoltage1);
		txtVoltage1.setColumns(10);

		JLabel lblR = new JLabel("Resistance");
		lblR.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblR.setBounds(12, 91, 73, 19);
		panel.add(lblR);

		txtResistance1 = new JTextField();
		txtResistance1.addKeyListener(new TxtResistanceKeyListener());
		txtResistance1.setToolTipText("Enter resistance for Circuit 1.");

		txtResistance1.setBounds(114, 90, 116, 22);
		panel.add(txtResistance1);
		txtResistance1.setColumns(10);

		JLabel lblCircuit = new JLabel("Circuit 1");
		lblCircuit.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCircuit.setBounds(12, 13, 76, 16);
		panel.add(lblCircuit);

		JLabel label = new JLabel("Location");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(12, 133, 76, 16);
		panel.add(label);

		comboBox = new JComboBox<String>();
		comboBox.setToolTipText("Select location for Circuit 1.");
		comboBox.addKeyListener(new TxtDoubleFieldKeyListener());
		comboBox.setModel(new DefaultComboBoxModel<>(COUNTRIES));
		comboBox.setBounds(114, 131, 116, 22);
		panel.add(comboBox);

		btnSave = new JButton("Save");
		btnSave.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
					clear();
				}
			}
		});
		btnSave.setToolTipText("Press Enter to save toy.");
		btnSave.addActionListener(new BtnSaveActionListener());
		btnSave.setBounds(180, 461, 97, 25);
		frame.getContentPane().add(btnSave);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(341, 230, 239, 193);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);

		JLabel lblVoltage = new JLabel("Voltage");
		lblVoltage.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblVoltage.setBounds(12, 57, 80, 16);
		panel_1.add(lblVoltage);

		txtVoltage2 = new JTextField();
		txtVoltage2.setToolTipText("Enter voltage for Circuit 2.");
		txtVoltage2.addKeyListener(new TxtDoubleFieldKeyListener());
		txtVoltage2.setBounds(111, 55, 116, 22);
		panel_1.add(txtVoltage2);
		txtVoltage2.setColumns(10);

		JLabel lblResistance = new JLabel("Resistance");
		lblResistance.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblResistance.setBounds(12, 93, 87, 19);
		panel_1.add(lblResistance);

		txtResistance2 = new JTextField();
		txtResistance2.setToolTipText("Enter resistance for Circuit 2.");
		txtResistance2.addKeyListener(new TxtResistanceKeyListener());
		txtResistance2.setBounds(111, 92, 116, 22);
		panel_1.add(txtResistance2);
		txtResistance2.setColumns(10);

		JLabel lblCircuit_1 = new JLabel("Circuit 2");
		lblCircuit_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCircuit_1.setBounds(12, 13, 76, 16);
		panel_1.add(lblCircuit_1);

		JLabel lblLocation = new JLabel("Location");
		lblLocation.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLocation.setBounds(12, 133, 76, 16);
		panel_1.add(lblLocation);

		comboBox_1 = new JComboBox<String>();
		comboBox_1.setToolTipText("Select location for Circuit 2.");
		comboBox_1.addKeyListener(new TxtDoubleFieldKeyListener());
		comboBox_1.setModel(new DefaultComboBoxModel<>(COUNTRIES));
		comboBox_1.setBounds(111, 127, 116, 22);
		panel_1.add(comboBox_1);

		JButton btnClear = new JButton("Clear");
		btnClear.setToolTipText("Press ESC to clear data.");
		btnClear.addActionListener(new BtnClearActionListener());
		btnClear.setBounds(359, 461, 97, 25);
		frame.getContentPane().add(btnClear);

		JLabel lblToyId = new JLabel("Toy ID:");
		lblToyId.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblToyId.setBounds(89, 112, 56, 16);
		frame.getContentPane().add(lblToyId);

		txtToyID = new JTextField();
		txtToyID.setToolTipText("This is the Toy ID on your toy.");
		txtToyID.addKeyListener(new TxtToyIDKeyListener());
		txtToyID.setBounds(203, 110, 206, 22);
		frame.getContentPane().add(txtToyID);
		txtToyID.setColumns(10);

		frame.getRootPane().setDefaultButton(btnSave);

		btnLoad = new JButton("Load Toy");
		btnLoad.setEnabled(false);
		btnLoad.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				if (arg0.getKeyChar() == KeyEvent.VK_ESCAPE) {
					clear();
				}
			}
		});
		btnLoad.setToolTipText("Hold alt+L to load toy.");
		btnLoad.setMnemonic('L');
		btnLoad.addActionListener(new BtnLoadActionListener());
		btnLoad.setBounds(456, 109, 97, 25);
		frame.getContentPane().add(btnLoad);

		btnDelete = new JButton("Delete Toy");
		btnDelete.setEnabled(false);
		btnDelete.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
					clear();
				}
			}
		});
		btnDelete.setToolTipText("Hold alt+d to delete toy.");
		btnDelete.setMnemonic('D');
		btnDelete.addActionListener(new BtnDeleteActionListener());
		btnDelete.setBounds(456, 162, 97, 25);
		frame.getContentPane().add(btnDelete);

		JLabel lblThisProgramWill = new JLabel(
				"This program will save the following information to cs.cofo.edu database.");
		lblThisProgramWill.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblThisProgramWill.setBounds(39, 13, 541, 32);
		frame.getContentPane().add(lblThisProgramWill);
	}

	// code- controller
	private class BtnSaveActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			if (txtInspector.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Please enter an inspector name.");

			} else {

				try {
					Toy myToy = new Toy();

					// set all toy only values
					myToy.setInspector(txtInspector.getText());
					myToy.setInspectionDateTime(new Date());
					myToy.setToyID(Integer.parseInt(txtToyID.getText()));

					// set circuit1 values
					myToy.getCircuit1().setVoltage(Double.parseDouble(txtVoltage1.getText()));
					myToy.getCircuit1().setResistance(Double.parseDouble(txtResistance1.getText()));
					myToy.getCircuit1().setManufactureLocation(comboBox.getSelectedItem().toString());
					myToy.getCircuit1().calculateAmperage();

					// set circuit 2
					myToy.getCircuit2().setVoltage(Double.parseDouble(txtVoltage2.getText()));
					myToy.getCircuit2().setResistance(Double.parseDouble(txtResistance2.getText()));
					myToy.getCircuit2().setManufactureLocation(comboBox_1.getSelectedItem().toString());
					myToy.getCircuit2().calculateAmperage();

					myToy.save();

				} catch (NumberFormatException f) {
					JOptionPane.showMessageDialog(null, "Please fill in all fields.");
				}
			}

		}
	}

	private class TxtDoubleFieldKeyListener extends KeyAdapter {
		@Override
		public void keyTyped(KeyEvent arg0) {
			if (arg0.getKeyChar() < KeyEvent.VK_0 || arg0.getKeyChar() > KeyEvent.VK_9) {
				if (arg0.getKeyChar() != KeyEvent.VK_PERIOD) {
					arg0.consume();
				}
			}
			if (arg0.getKeyChar() < KeyEvent.VK_0 || arg0.getKeyChar() > KeyEvent.VK_9) {
				if (arg0.getKeyChar() != KeyEvent.VK_PERIOD
						|| ((JTextField) arg0.getSource()).getText().contains(".")) {
					arg0.consume();
				}
				if (arg0.getKeyChar() == KeyEvent.VK_ESCAPE) {
					clear();
				}
			}
			
		}

	} // end of event

	private class BtnClearActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			clear();
		}
	}

	private class TxtToyIDKeyListener extends KeyAdapter {
		@Override
		public void keyTyped(KeyEvent a) {
			if (a.getKeyChar() == KeyEvent.VK_ESCAPE) {
				clear();
			}
			if (a.getKeyChar() < KeyEvent.VK_0 || a.getKeyChar() > KeyEvent.VK_9) {
				a.consume();
			}
			
		}

		@Override
		public void keyReleased(KeyEvent e) {
			if (txtToyID.getText().equals("")) {
				btnLoad.setEnabled(false);
				btnDelete.setEnabled(false);
			} else {
				btnLoad.setEnabled(true);
				btnDelete.setEnabled(true);
			}
		}
	}

	private class BtnLoadActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				myToy.load(Integer.parseInt(txtToyID.getText()));

				txtToyID.setText(String.valueOf(myToy.getToyID()));
				txtInspector.setText(myToy.getInspector());
				txtDate.setText(String.valueOf(myToy.getInspectionDateTime()));

				txtVoltage1.setText(String.valueOf(myToy.getCircuit1().getVoltage()));
				txtResistance1.setText(String.valueOf(myToy.getCircuit1().getResistance()));
				comboBox.setSelectedItem(myToy.getCircuit1().getManufactureLocation());

				txtVoltage2.setText(String.valueOf(myToy.getCircuit2().getVoltage()));
				txtResistance2.setText(String.valueOf(myToy.getCircuit2().getResistance()));
				comboBox_1.setSelectedItem(myToy.getCircuit2().getManufactureLocation());

			} catch (InvalidParameterException arg0) {
				JOptionPane.showMessageDialog(null, "ToyID not found!");
				arg0.getMessage();

			}

		}
	}

	private class BtnDeleteActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (myToy.getToyID() != 0) {
				if (JOptionPane.showConfirmDialog(null, "Are you sure you want to delete the loaded toy?",
						"Confirm Delete", JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
					myToy.delete();
					clear();
					btnDelete.setEnabled(false);
					btnLoad.setEnabled(false);

				}
			}
		}
	}

	private class TxtInspectorKeyListener extends KeyAdapter {
		@Override
		public void keyTyped(KeyEvent e) {
			if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
				clear();
			}
		}
	}

	private class TxtResistanceKeyListener extends KeyAdapter {
		@Override
		public void keyTyped(KeyEvent arg0) {
			if (arg0.getKeyChar() == KeyEvent.VK_0) {
				arg0.consume();
			}
			if (arg0.getKeyChar() < KeyEvent.VK_0 || arg0.getKeyChar() > KeyEvent.VK_9) {
				if (arg0.getKeyChar() != KeyEvent.VK_PERIOD) {
					arg0.consume();
				}
			}
			if (arg0.getKeyChar() < KeyEvent.VK_0 || arg0.getKeyChar() > KeyEvent.VK_9) {
				if (arg0.getKeyChar() != KeyEvent.VK_PERIOD
						|| ((JTextField) arg0.getSource()).getText().contains(".")) {
					arg0.consume();
				}
				if (arg0.getKeyChar() == KeyEvent.VK_ESCAPE) {
					clear();
				}
			}
		}
	}

	private void clear() {
		txtVoltage1.setText("");
		txtVoltage2.setText("");
		txtResistance1.setText("");
		txtResistance2.setText("");
		txtToyID.setText("");
		txtToyID.requestFocus();
		comboBox.setSelectedItem("China");
		comboBox_1.setSelectedItem("China");

	}
}// end of master class
